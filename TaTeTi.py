from PyQt5.QtWidgets            import QMainWindow, QApplication
from ventana.Main.Main          import Main
from clases.Class_BDSqlite      import BD_Slite


# Creacion de Base de Datos
BaseDeDatos_nombre = "data.sql"
BaseDeDatos = BD_Slite(BaseDeDatos_nombre)
BaseDeDatos.db_connect()
BaseDeDatos.db_create()


app = QApplication([])

win = Main(BaseDeDatos_nombre)
win.show()
app.exec_()