from PyQt5.QtWidgets import QMainWindow, QDialog, QApplication
from PyQt5 import uic
from PyQt5.QtGui import QIcon
from clases.Class_BDSqlite            import BD_Slite
from clases.ClassPlayers              import player


class Eliminar(QMainWindow):
    def __init__(self, BaseDeDatos_nombre):
        super().__init__()
        uic.loadUi("ventana\Eliminar/Eliminar.ui", self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        with open("styles.css") as f:
            self.setStyleSheet(f.read())
        self.BaseDeDatos_nombre = BaseDeDatos_nombre
        self.BaseDeDatos = BD_Slite(BaseDeDatos_nombre)
        self.BaseDeDatos.db_connect()
        self.QPushButton_Eliminar.clicked.connect(self.eliminar_player)
        self.QPushButton_Cancelar.clicked.connect(self.close)


    def id_eliminar(self, id_encontrado):
        self.id_encontrado = id_encontrado   

    def eliminar_player(self):
        self.BaseDeDatos.db_delete_PlayerData (self.id_encontrado)
        self.BaseDeDatos.db_delete_PlayerRecord (self.id_encontrado)
        self.close()