from PyQt5.QtWidgets            import QMainWindow, QApplication,QHeaderView,QTableWidgetItem
from PyQt5                      import uic
from PyQt5.QtGui                import QIcon
from clases.Class_BDSqlite      import BD_Slite
from clases.ClassPlayers        import player


class TablaPuntaje(QMainWindow):
    def __init__(self,BaseDeDatos_nombre):
        super().__init__()
        self.BaseDeDatos_nombre = BaseDeDatos_nombre
        self.BaseDeDatos = BD_Slite(self.BaseDeDatos_nombre)
        self.BaseDeDatos.db_connect()
        uic.loadUi("ventana\Tabla_puntaje/tabla_puntaje.ui", self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        #Abrir la hoja de stylo en CSS
        with open("styles.css") as f:
            self.setStyleSheet(f.read())        
        self.QPushButton_Cerrar.clicked.connect  (self.CloseWindow)
        self.CreateTable()
        self.InsertTablaData()

    def CreateTable(self):
        self.Columnas = ("Nick;N° de Partidas;Partidas Ganadas;Partidas Empatadas;Partidas Perdidas; Puntaje Acumulado").split(";")
        # Creacion de las Columnas
        self.QTableWidget_Tabla.setColumnCount(len(self.Columnas))
        # Creacion de los Labels de las Columnas
        self.QTableWidget_Tabla.setHorizontalHeaderLabels(self.Columnas)
        # Distribuye uniformemente  el tamaño de las columnas.
        for i in range (0,len(self.Columnas),1):
            self.QTableWidget_Tabla.horizontalHeader().setSectionResizeMode(i,QHeaderView.Stretch) 
       
    def CloseWindow(self):
        self.close()

    def InsertTablaData(self):
        # Lista de Jugadores
        Lista_Jugadores=[]
        # Traemos de la Base de Datos a Todos los Jugadores
        Jugadores=self.BaseDeDatos.AllPlayersData()
        # Agregamoso a la Lista de Jugadores a cada jugador de la Base de Dato
        for jugador in Jugadores:
            Lista_Jugadores.append (player(jugador[0],jugador[1],jugador[2],jugador[3],jugador[4],jugador[5],jugador[6],jugador[7],jugador[8],jugador[9]))
        
        # Ordeno la lista segun el puntaje de cada jugador
        Lista_Jugadores.sort(reverse=True)
 
        # Cargamos la tabla con los Jugadores   
        # Por cada ciclo del For agrego una fila a la Tabla, y en cada fila cargo los datos segun la columna
        self.QTableWidget_Tabla.setRowCount(0)
        for  fila in range (0,len (Lista_Jugadores),1):
            self.QTableWidget_Tabla.insertRow(fila) #Crea la Fila
            # Inserta en la fila,columna,dato del jugador
            self.QTableWidget_Tabla.setItem(fila,0,QTableWidgetItem(Lista_Jugadores[fila].nick))
            self.QTableWidget_Tabla.setItem(fila,1,QTableWidgetItem(str(Lista_Jugadores[fila].Paridos_Jugados())))
            self.QTableWidget_Tabla.setItem(fila,2,QTableWidgetItem(str(Lista_Jugadores[fila].PG)))
            self.QTableWidget_Tabla.setItem(fila,3,QTableWidgetItem(str(Lista_Jugadores[fila].PE)))
            self.QTableWidget_Tabla.setItem(fila,4,QTableWidgetItem(str(Lista_Jugadores[fila].PP)))
            self.QTableWidget_Tabla.setItem(fila,5,QTableWidgetItem(str(Lista_Jugadores[fila].puntos)))