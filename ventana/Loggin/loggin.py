from PyQt5.QtWidgets                                import QMainWindow, QApplication, QMessageBox
from PyQt5.QtGui                                    import QFont,QFontDatabase, QIcon
from PyQt5                                          import uic
from clases.Class_BDSqlite                          import BD_Slite
from clases.ClassPlayers                            import player
from ventana.Eliminar.Eliminar                      import Eliminar
from ventana.Registro_de_usuario.registro           import VentanaRegistro

class formulario(QMainWindow):
    def __init__(self, BaseDeDatos_nombre):
        super().__init__()
        self.BaseDeDatos_nombre = BaseDeDatos_nombre
        self.BaseDeDatos = BD_Slite(BaseDeDatos_nombre)
        self.BaseDeDatos.db_connect()
        uic.loadUi("ventana\Loggin/loggin.ui", self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        #Abrir la hoja de stylo en CSS
        with open("styles.css") as f:
            self.setStyleSheet(f.read())
        self.pushButton_Aceptar.clicked.connect(self.validar_pass)
        self.pushButton_Cancelar.clicked.connect(self.cerrar)
        self.ventana_eliminar = Eliminar (self.BaseDeDatos_nombre)
        self.ventana_registro = VentanaRegistro (self.BaseDeDatos_nombre)
        
    def limpiar_pantalla(self):
        self.lineEdit_Nick.clear()
        self.lineEdit_Password.clear()

    def validar_pass(self):
        nick = self.lineEdit_Nick.text()
        contrasenia = self.lineEdit_Password.text()
        Validado = False   

        Lista_Jugadores=[]
        # Traemos de la Base de Datos a Todos los Jugadores
        Jugadores = self.BaseDeDatos.AllPlayersData()
        # Agregamoso a la Lista de Jugadores a cada jugador de la Base de Dato
        for jugador in Jugadores:
            Lista_Jugadores.append (player(jugador[0],jugador[1],jugador[2],jugador[3],jugador[4],jugador[5],jugador[6],jugador[7],jugador[8],jugador[9]))
        #print(Lista_Jugadores)
        Validado = False
        for item in Lista_Jugadores:
            if nick == item.nick and contrasenia == item.password:
                Validado = True 
                self.id_encontrado = item.id    

        if Validado == True:
           #posible funcion para id encontrado registrar
            if self.origen == "Eliminar":
                self.ventana_eliminar.id_eliminar(self.id_encontrado)
                self.ventana_eliminar.show()
                self.limpiar_pantalla()
                self.close()
            
            elif self.origen == "Modificar":
                self.ventana_registro.ID (self.id_encontrado)
                self.ventana_registro.modificar_crear("Modificar") 
                self.ventana_registro.abrir_ventana()
                self.limpiar_pantalla()
                self.close()

        elif Validado == False:
            msg = QMessageBox()
            with open("styles.css") as f:
                msg.setStyleSheet(f.read())
            font_name="Kodchasan-SemiBold"
            QFontDatabase.addApplicationFont("fuentes/Kodchasan-SemiBold.ttf")
            fuente = QFont(font_name)
            msg.setFont(fuente)             
            #Titulo
            msg.setWindowTitle("Error")
            #Icono
            msg.setWindowIcon(QIcon("imagenes/tateti.png"))
            #Cuerpo
            Texto="Nick o Contraseña incorrectos"
            msg.setText(Texto)
            #Icono
            msg.setIcon(QMessageBox.NoIcon)
            #Botones
            msg.exec_()
    
    def entrada(self, origen):
        self.origen = origen 

    def cerrar (self)    :
        self.limpiar_pantalla()
        self.close()