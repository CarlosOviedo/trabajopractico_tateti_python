from PyQt5.QtWidgets            import QMainWindow, QApplication, QMessageBox
from PyQt5.QtGui                import QFont,QFontDatabase, QIcon
from PyQt5                      import uic,QtCore
from clases.Class_BDSqlite      import BD_Slite
from clases.ClassPlayers        import player
from random                     import randrange


class VentanaTablero(QMainWindow):
    def __init__(self, BaseDeDatos_nombre , Player_Uno, Player_Dos,Tipo_Partida):
        super().__init__()
        self.Player_Uno                 = Player_Uno
        self.Player_Dos                 = Player_Dos
        self.Tipo_Partida               = Tipo_Partida
        self.BaseDeDatos_nombre         = BaseDeDatos_nombre
        self.BaseDeDatos_nombre         = BaseDeDatos_nombre
        self.BaseDeDatos                = BD_Slite(self.BaseDeDatos_nombre)
        self.BaseDeDatos.db_connect()

        uic.loadUi("ventana/Tablero/tablero.ui",self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        with open("styles.css") as f:
            self.setStyleSheet(f.read())   
        self.gameFinished   = False
        self.TurnoX         = True
        self.position       = 0  
        self.board          = [' ']*9
        self.pushButton_1.clicked.connect(self.btn1)
        self.pushButton_2.clicked.connect(self.btn2)
        self.pushButton_3.clicked.connect(self.btn3)
        self.pushButton_4.clicked.connect(self.btn4)
        self.pushButton_5.clicked.connect(self.btn5)
        self.pushButton_6.clicked.connect(self.btn6)
        self.pushButton_7.clicked.connect(self.btn7)
        self.pushButton_8.clicked.connect(self.btn8)
        self.pushButton_9.clicked.connect(self.btn9)
        self.Qlabel_nick1.setText(self.Player_Uno.nick)
        self.Qlabel_nick2.setText(self.Player_Dos.nick)
        # Configuro el Timer
        self.timer = QtCore.QTimer()
        self.now = 0
        # Inicio la cuenta a Cero
        
        # Configuro el Timer a que cuente un segundo
        self.timer.start(1000) #ms .. entonces 1S: 1000ms
        # Configuro que cuando llegue al segundo ejecute la funcion
        self.timer.timeout.connect(self.tick_timer)
    
    # Funcion del Timer
    def tick_timer(self):
        # Actualizo la cuenta actual y Refresco el LCD
        if self.gameFinished == False:
            self.now += 1
            self.lcd_timer_update()
    # Refresco LCD

    def lcd_timer_update(self):
        self.runtime = "%d:%02d" % (self.now/60,self.now % 60)
        self.QLCDNumber_Tiempo.display(self.runtime)    
    
    def btn1(self):
        self.position       =(1)
        self.botonPresionado (self.pushButton_1)
        self.game (self.position)       
        
    def btn2(self):
        self.position       =(2)
        self.botonPresionado (self.pushButton_2)
        self.game (self.position)   

    def btn3(self):
        self.position       =(3)
        self.botonPresionado (self.pushButton_3) 
        self.game (self.position)   

    def btn4(self):
        self.position       =(4)
        self.botonPresionado (self.pushButton_4)
        self.game (self.position)   

    def btn5(self):
        self.position       =(5)
        self.botonPresionado (self.pushButton_5)
        self.game (self.position)  

    def btn6(self):
        self.position       =(6)
        self.botonPresionado (self.pushButton_6)
        self.game (self.position)  

    def btn7(self):
        self.position       =(7)
        self.botonPresionado (self.pushButton_7)
        self.game (self.position)     

    def btn8(self):
        self.position       =(8)
        self.botonPresionado (self.pushButton_8)
        self.game (self.position)  

    def btn9(self):
        self.position       =(9)
        self.botonPresionado (self.pushButton_9)
        self.game (self.position)            
    
    def botonPresionado (self,boton):
        if not self.gameFinished:
            if self.TurnoX:
                boton.setText("x")
                
            else:
                boton.setText("o")
            boton.setEnabled(False)

    def WindowResult(self,jugador):
        # Ventana Emergente
        msg = QMessageBox()
        with open("styles.css") as f:
            msg.setStyleSheet(f.read())
        font_name="Kodchasan-SemiBold"
        QFontDatabase.addApplicationFont("fuentes/Kodchasan-SemiBold.ttf")
        fuente = QFont(font_name)
        msg.setFont(fuente)  
        #Titulo
        msg.setWindowTitle("Resultado")
        msg.setWindowIcon(QIcon("imagenes/tateti.png"))

        #Cuerpo
        if (jugador == "X"):
            msg.setText("El ganador es: "+ self.Player_Uno.nick)
        
        elif(jugador == "O"):
            msg.setText("El ganador es: "+ self.Player_Dos.nick)
        else:
            msg.setText("Fue un empate")   
        #Icono
        msg.setIcon(QMessageBox.Information)
        #Botones
        msg.setStandardButtons(QMessageBox.Ok )
        msg.exec_()
     
    def game (self,position):
        if not self.gameFinished:
            if self.TurnoX:
                self.board[position-1]="X"
            else:
                self.board[position-1]="O"

            # Condiciones para Ganar la partida
            if(self.board[0] == self.board[1] == self.board[2] and self.board[0]!=' ' or 
               self.board[3] == self.board[4] == self.board[5] and self.board[3]!=' ' or
               self.board[6] == self.board[7] == self.board[8] and self.board[6]!=' ' or    
               self.board[0] == self.board[3] == self.board[6] and self.board[0]!=' ' or
               self.board[1] == self.board[4] == self.board[7] and self.board[1]!=' ' or 
               self.board[2] == self.board[5] == self.board[8] and self.board[2]!=' ' or 
               self.board[0] == self.board[4] == self.board[8] and self.board[0]!=' ' or 
               self.board[2] == self.board[4] == self.board[6] and self.board[2]!=' '   ):
               # Si se cumple la condicion de ganar en el turno de X
               if self.TurnoX: 
                   # Gana el Player Uno, Pierde el Player Dos
                   self.finshgame(self.Player_Uno,self.Player_Dos)
                   self.WindowResult("X")
               # Si se cumple la condicion de ganar en el turno de O
               else:
                   # Gana el Player Dos, Pierde el Player Uno
                   self.finshgame(self.Player_Dos,self.Player_Uno)
                   self.WindowResult("O")
               
               self.gameFinished = True
            # Si no hay espacio vacio en el board es un empate
            if (self.gameFinished == False) and (' ' not in self.board):
                self.WindowResult("Empate") 
                self.gameFinished= True
                self.empate(self.Player_Uno,self.Player_Dos)
            
            self.TurnoX= not self.TurnoX
            # Si no es el turno de X y el player dos es la PC. Ejecuto PC_Play
            if self.TurnoX == False and self.Player_Dos.nick == "PC":
                self.PC_Play ()
    def empate(self,Player_Uno,Player_Dos):
        if self.Tipo_Partida == "Campeonato":
            Resultado_JugadorUno = "E"
            Resultado_JugadorDos = "E"
            self.Player_Uno.PE=self.Player_Uno.PE+1
            self.Player_Dos.PE=self.Player_Dos.PE+1 
            #Cargo el juego realizado en la Base de Datos
            self.BaseDeDatos.NewGame (0,self.Player_Uno.id,self.Player_Dos.id,Resultado_JugadorUno,Resultado_JugadorDos)
            #Genero el Puntaje con el metodo de la clase Player
            self.Player_Uno.Puntos()
            self.Player_Dos.Puntos()
            #Cargo el resultado del Juego en la base de datos
            self.BaseDeDatos.NewGameResult (self.Player_Uno.id,self.Player_Uno.PG,self.Player_Uno.PP,self.Player_Uno.PE,self.Player_Uno.puntos,self.Player_Dos.id,self.Player_Dos.PG,self.Player_Dos.PP,self.Player_Dos.PE,self.Player_Dos.puntos)        

    def finshgame(self,Player_Ganador,Player_Perdedor):
        if self.Tipo_Partida == "PartidaRapida":
        # Si estamos en MODO PartidaRapida no se guarda la partida     
            print ("GANADOR: ",Player_Ganador.nick)
            print ("PERDEDOR: ",Player_Perdedor.nick)

        elif self.Tipo_Partida == "Campeonato":
        # Si estamos en MODO Campeonato se guarda la partida
        # Comparo el nick del jugador con el nick del player ganador.    
            if self.Player_Uno.nick == Player_Ganador.nick:
                Resultado_JugadorUno = "V"
                self.Player_Uno.PG=self.Player_Uno.PG+1
                
            elif self.Player_Uno.nick == Player_Perdedor.nick:
                Resultado_JugadorUno = "P"
                self.Player_Uno.PP=self.Player_Uno.PP+1   
        
            if self.Player_Dos.nick == Player_Ganador.nick:
                Resultado_JugadorDos = "V"
                self.Player_Dos.PG=self.Player_Dos.PG+1
        
            elif self.Player_Dos.nick == Player_Perdedor.nick:
                Resultado_JugadorDos = "P"
                self.Player_Dos.PP=self.Player_Dos.PP+1 
            #Cargo el juego realizado en la Base de Datos
            self.BaseDeDatos.NewGame (0,self.Player_Uno.id,self.Player_Dos.id,Resultado_JugadorUno,Resultado_JugadorDos)
            #Genero el Puntaje con el metodo de la clase Player
            self.Player_Uno.Puntos()
            self.Player_Dos.Puntos()
            #Cargo el resultado del Juego en la base de datos
            self.BaseDeDatos.NewGameResult (self.Player_Uno.id,self.Player_Uno.PG,self.Player_Uno.PP,self.Player_Uno.PE,self.Player_Uno.puntos,self.Player_Dos.id,self.Player_Dos.PG,self.Player_Dos.PP,self.Player_Dos.PE,self.Player_Dos.puntos)

    def PC_Play (self):
        # Juega la PC 
        # Selecciono la casillera del medio para comenzar
        casillero = 5
        while ((self.gameFinished== False) and ((self.board[casillero-1] == "X") or (self.board[casillero-1] == "O"))):
            # Si el casillero en el board esta ocupado busco otro numero 
            casillero = randrange(1, 9)
        # Un vez obtenido el numero ejecuto la funcion al boton correspondiente
        if casillero    == 1:
            self.btn1()
        elif casillero  == 2:
            self.btn2()
        elif casillero  == 3:
            self.btn3() 
        elif casillero  == 4:
            self.btn4()
        elif casillero  == 5:
            self.btn5()  
        elif casillero  == 6:
            self.btn6()  
        elif casillero  == 7:
            self.btn7() 
        elif casillero  == 8:
            self.btn8()  
        elif casillero  == 9:
            self.btn9() 