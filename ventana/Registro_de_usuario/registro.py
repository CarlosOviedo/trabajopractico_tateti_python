from PyQt5.QtWidgets            import QMainWindow, QApplication, QMessageBox
from PyQt5.QtGui                import QFont,QFontDatabase, QIcon
from PyQt5                              import uic
from clases.Class_BDSqlite              import BD_Slite

class VentanaRegistro(QMainWindow):
    def __init__(self, BaseDeDatos_nombre):
        super().__init__()
        # En esta variable esta el nobre del archivo data.sql
        self.BaseDeDatos_nombre = BaseDeDatos_nombre
        # creo las variables refiridas a la clase Class_BDSqlite
        self.BaseDeDatos = BD_Slite(self.BaseDeDatos_nombre)
        # db_connect es un meto de la clase BD_Slite
        self.BaseDeDatos.db_connect()
        self.origen = "Crear"
        self.id_encontrado = -1
    
        uic.loadUi("ventana/Registro_de_usuario/RegistroDeUsuario.ui", self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        with open("styles.css") as f:
            self.setStyleSheet(f.read())
      
        self.aceptar_QpushButton.clicked.connect(self.enviar_datos) 
        self.cancelar_QpushButton.clicked.connect(self.close) 
    
    def abrir_ventana(self):
        self.nick_QlineEdit.clear()
        self.email_QlineEdit.clear()
        self.password_QlineEdit.clear()
        self.QspinBox_edad.setValue(0)


        if self.origen == "Modificar":
            self.mostrar_datos()
        self.show()   

    def enviar_datos(self):
        nick = self.nick_QlineEdit.text()
        correo = self.email_QlineEdit.text()
        contraseña = self.password_QlineEdit.text()
        edad = self.QspinBox_edad.value()
        sex_female = self.radioButton_femenino.isChecked()
        sex_male = self.radioButton_masculino.isChecked()
        if nick != "" and contraseña != "" and correo != "" and edad != 0: 
            if self.origen != "Modificar":
                if sex_female==True:
                    sexo = 'F'
                elif sex_male == True:
                    sexo = 'M'         
                self.BaseDeDatos.NewPlayer(correo,contraseña,nick,edad,sexo)
                self.nick_QlineEdit.setText('')
                self.email_QlineEdit.setText('')
                self.password_QlineEdit.setText('')
                self.QspinBox_edad.setValue(0)
                self.close()
            else:
                self.modificar_datos()
                self.close()
        else:
            msg = QMessageBox()
            with open("styles.css") as f:
                msg.setStyleSheet(f.read())
            font_name="Kodchasan-SemiBold"
            QFontDatabase.addApplicationFont("fuentes/Kodchasan-SemiBold.ttf")
            fuente = QFont(font_name)
            msg.setFont(fuente)
            #Titulo
            msg.setWindowTitle("Error")
            
            #Icono
            msg.setWindowIcon(QIcon("imagenes/tateti.png"))

            #Cuerpo
            msg.setText("Faltan Cargar Datos")
            msg.setIcon(QMessageBox.Warning)
            #Botones
            msg.setStandardButtons(QMessageBox.Ok )
            msg.exec_()
    
    def mostrar_datos(self):
        player = self.BaseDeDatos.Serch_PlayerData(self.id_encontrado)
       
        self.nick_QlineEdit.setText(player[0][1])    #Esto esta tirando error, los datos los levanta de la base pero no los levanta en la ventana    
        self.email_QlineEdit.setText(player[0][4])
        self.password_QlineEdit.setText(player[0][5])
        self.QspinBox_edad.setValue(int (player[0][2]))
        if (player[0][3]) == 'M':
            self.radioButton_masculino.setChecked(True)
        else:
            self.radioButton_femenino.setChecked(True)

   
    def modificar_datos(self):
        nick = self.nick_QlineEdit.text()
        correo = self.email_QlineEdit.text()
        contraseña = self.password_QlineEdit.text()
        edad = self.QspinBox_edad.value()
        sex_female = self.radioButton_femenino.isChecked()
        if sex_female==True:
            sexo = 'F'
        sex_male = self.radioButton_masculino.isChecked()
        if sex_male == True:
            sexo = 'M'
                                
        self.BaseDeDatos.db_update_PlayerData (self.id_encontrado,correo,contraseña,nick,edad,sexo)

        self.nick_QlineEdit.setText('')
        self.email_QlineEdit.setText('')
        self.password_QlineEdit.setText('')
        self.QspinBox_edad.setValue(0)

        self.close()

      
    def modificar_crear(self, origen):
        self.origen = origen 

    def ID (self, id_encontrado):
        self.id_encontrado = id_encontrado    