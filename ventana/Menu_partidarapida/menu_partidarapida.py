from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
from PyQt5.QtGui import QIcon
from ventana.Players.players import ventanaPlayer

class PartidaRapida(QMainWindow):
    def __init__(self,BaseDeDatos_nombre):
        super().__init__()
        self.BaseDeDatos_nombre = BaseDeDatos_nombre

        uic.loadUi("ventana\Menu_partidarapida/menu_partidarapida.ui", self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        with open("styles.css") as f:
            self.setStyleSheet(f.read())
        self.window_players = ventanaPlayer(self.BaseDeDatos_nombre)
        self.QPushButton_dosjugadores.clicked.connect(self.open_window_DosJugadores)
        self.QPushButton_vsPC.clicked.connect(self.open_window_vsPC)
    
    def open_window_DosJugadores(self):
        self.window_players.tipo_juego("HumanoVsHumano")
        self.window_players.show()
        self.close()

    def open_window_vsPC(self):
        self.window_players.tipo_juego("HumanoVsPC")
        self.window_players.show()
        self.close()
