from PyQt5.QtWidgets                                import QMainWindow, QApplication,QHeaderView,QTableWidgetItem, QMessageBox
from PyQt5.QtGui                                    import QFont,QFontDatabase, QIcon
from PyQt5                                          import uic
from ventana.Tablero.Tablero                        import VentanaTablero
from clases.Class_BDSqlite                          import BD_Slite
from clases.ClassPlayers                            import player
from ventana.Registro_de_usuario.registro           import VentanaRegistro
class Menu_campeonato(QMainWindow):
    def __init__(self,BaseDeDatos_nombre):
        super().__init__()
        uic.loadUi("ventana\Menu_campeonato/menu_campeonato.ui", self)
        self.setWindowIcon(QIcon("imagenes/tateti.png"))

        #Abrir la hoja de stylo en CSS
        with open("styles.css") as self.f:
            self.setStyleSheet(self.f.read())
        
        self.BaseDeDatos_nombre = BaseDeDatos_nombre
        self.boton_crear_jugar = False
        self.partida = 0
        self.BaseDeDatos = BD_Slite(self.BaseDeDatos_nombre)
        self.BaseDeDatos.db_connect()
        self.Window_Registro        = VentanaRegistro                   (self.BaseDeDatos_nombre)

        self.QPushButton_AgregarJugador.clicked.connect                 (self.AgregarJugador)
        self.QPushButton_QuitarJugador.clicked.connect                  (self.QuitarJugador)
        self.QPushButton_Cancelar.clicked.connect                       (self.Cancelar)

        self.QpushButton_CrearCampeonato_Jugar.clicked.connect          (self.CrearCampeonato_JugarPartida)
        self.QSpinBox_CantidadJugadores.valueChanged.connect            (self.CantidadJugadoresChange)
        self.CargaListaPlayers()
        self.CreateTable ()

    def Cancelar (self):
        self.close()

    def CantidadJugadoresChange (self):
        # Si se cambia el valor del numero de jugadores se re inicia Todo.
        # Se crea la tabla de partidas
        self.CreateTable ()
        # Se Elimina todos los jugadores seleccionados
        while (self.QListWidget_Seleccionados.count()>0):
           self.QListWidget_Seleccionados.takeItem(self.QListWidget_Seleccionados.count()-1)   
        # Se Modifica el boton "Crear Campeonato"
        self.boton_crear_jugar = False
        self.QpushButton_CrearCampeonato_Jugar.setText("Crear Campeonato")

    def CrearCampeonato_JugarPartida(self):
        # Si no se presiono el boton antes:
        if self.boton_crear_jugar == False:
            # Verificamos que se hayan agregado jugadores 
            if self.QListWidget_Seleccionados.count() == 0 and self.QListWidget_Seleccionados.count() < 2:
                self.VentanaEmergente ("Error","Numero de Jugadores Selecionados Incorrecto")
            elif self.QListWidget_Seleccionados.count() >= 2: 
                # Cambiamos el Nombre al Boton por "Jugar"           
                self.QpushButton_CrearCampeonato_Jugar.setText("Jugar")
                self.TablaPartidas ()
                self.boton_crear_jugar = True
        # Si ya se presiono el boton previamente:      
        elif  self.boton_crear_jugar == True: 
            self.Jugar ()

    def CargaListaPlayers(self):
        self.QListWidget_Players.clear()
        # Lista de Jugadores
        self.Lista_Jugadores=[]
        # Traemos de la Base de Datos a Todos los Jugadores
        self.Jugadores=self.BaseDeDatos.AllPlayersData()
        # Agregamoso a la Lista de Jugadores a cada jugador de la Base de Dato
        for jugador in self.Jugadores:
            self.Lista_Jugadores.append (player(jugador[0],jugador[1],jugador[2],jugador[3],jugador[4],jugador[5],jugador[6],jugador[7],jugador[8],jugador[9]))
        # Agregamos los jugadores a la lista QListWidget_Players
        for item in self.Lista_Jugadores:
            self.QListWidget_Players.addItem(item.nick)

    def AgregarJugador(self):
        JugadorCargado= False
        # Tomamos el indici el jugador selecionado
        indice = self.QListWidget_Players.currentRow() 
        # Recorremos la lista de seleccionado para ver si estaba cargado
        for i in range (0, self.QListWidget_Seleccionados.count(),1):
            if (self.QListWidget_Players.item(indice).text()== self.QListWidget_Seleccionados.item(i).text()):
                JugadorCargado= True
        # Agregamos Jugador a la Lista QListWidget_Seleccionados segun quien esta seleccionado en la lista QListWidget_Players
        if JugadorCargado ==  False:
            if (self.QSpinBox_CantidadJugadores.value()-1>=self.QListWidget_Seleccionados.count()):
                self.QListWidget_Seleccionados.addItem(self.Lista_Jugadores[indice].nick)
        else:
            self.VentanaEmergente ("Error","El jugador ya se encuentra cargado")

    def QuitarJugador (self):
        # Se quita el jugador selecionado en la Lista Seleccionado, tomando el indice
        indice = self.QListWidget_Seleccionados.currentRow()
        self.QListWidget_Seleccionados.takeItem(indice)  
    
    def CreateTable (self):
        # Borramos la Tabla para comenzar 
        self.QTableWidget_Partidas.clear()
        # Si existieran filas cargadas (porque se jugo previamente un campeonato, se borran las filas)
        while self.QTableWidget_Partidas.rowCount() > 0:
            self.QTableWidget_Partidas.removeRow(self.QTableWidget_Partidas.rowCount()-1)
        # Seteamos las etiquetas de la tabla
        self.Columnas = (" Partida ; Jugador Uno ;    ; Jugador Dos ").split(";")
        # Creacion de las Columnas
        self.QTableWidget_Partidas.setColumnCount(len(self.Columnas))
        # Creacion de los Labels de las Columnas
        self.QTableWidget_Partidas.setHorizontalHeaderLabels(self.Columnas)
        # Distribuye uniformemente  el tamaño de las columnas.
        for i in range (0,len(self.Columnas),1):
            self.QTableWidget_Partidas.horizontalHeader().setSectionResizeMode(i,QHeaderView.Stretch) 
               
    def Jugar (self):
  
        for JUGADOR in self.Lista_Jugadores:
                # Comparamos el nick de la fila=partida y columna=1 de la Tabla Partidas
            if (JUGADOR.nick == self.QTableWidget_Partidas.item(self.partida, 1).text()):
                Player_Uno = JUGADOR
                # Comparamos el nick de la fila=partida y columna=3 de la Tabla Partidas
            if (JUGADOR.nick == self.QTableWidget_Partidas.item(self.partida, 3).text()):
                Player_Dos = JUGADOR
                # Se ejecuta el la ventana Tablero con los jugadores     
        self.window_tablero = VentanaTablero(self.BaseDeDatos_nombre,Player_Uno, Player_Dos,"Campeonato" )
        self.window_tablero.show()

        # Aumentamos el numero de partidas    
        self.partida = self.partida+1
        if self.partida == self.QTableWidget_Partidas.rowCount():
            self.partida = 0
            self.CreateTable ()
            self.QpushButton_CrearCampeonato_Jugar.setText("Crear Campeonato")
            self.boton_crear_jugar = False
        
    def TablaPartidas (self):
        # Se toma el numero de jugadores seleccionado
        numero_de_jugador = self.QListWidget_Seleccionados.count()
        fila = 0
        # Se carga la Tabla con el numero de Jugadores enfrentando uno por uno.
        for i in range (0,numero_de_jugador,1):
            for j in range (0,self.QListWidget_Seleccionados.count(),1):
                if i !=j:
                    # Se crea fila
                    self.QTableWidget_Partidas.insertRow(fila)
                    # Se inserta el numero de partida
                    self.QTableWidget_Partidas.setItem(fila,0,QTableWidgetItem(str(fila+1)))
                    # Se carga el nick del jugador uno
                    self.QTableWidget_Partidas.setItem(fila,1,QTableWidgetItem(self.QListWidget_Seleccionados.item(i).text()))
                    # Se carga el "vs"
                    self.QTableWidget_Partidas.setItem(fila,2,QTableWidgetItem(" vs "))
                    # Se carga el nick del jugador dos
                    self.QTableWidget_Partidas.setItem(fila,3,QTableWidgetItem(self.QListWidget_Seleccionados.item(j).text()))
                    # Se aumenta la fila
                    fila = fila+1

    def VentanaEmergente (self,Titulo,Texto):
        msg = QMessageBox()
        with open("styles.css") as f:
            msg.setStyleSheet(f.read())
        font_name="Kodchasan-SemiBold"
        QFontDatabase.addApplicationFont("fuentes/Kodchasan-SemiBold.ttf")
        fuente = QFont(font_name)
        msg.setWindowIcon(QIcon("imagenes/tateti.png"))
        msg.setFont(fuente)  
        #Titulo
        msg.setWindowTitle(Titulo)
        #Icono
        msg.setWindowIcon(QIcon("imagenes/tateti.png"))
        #Cuerpo
        msg.setText(Texto)   
        #Icono
        msg.setIcon(QMessageBox.Warning)
        #Botones
        msg.setStandardButtons(QMessageBox.Ok )
        msg.exec_()

    def AbrirVentana (self):
        self.QListWidget_Seleccionados.clear()
        self.CreateTable ()
        self.boton_crear_jugar = False
        self.CargaListaPlayers()
        self.show()


